# -*- coding: utf-8 -*-
#
# Copyright (c) 2019, 2021 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3.6 or newer

import asyncio
import concurrent.futures
import os
from typing import Any, Dict, Mapping, Optional

import aiohttp
import gidgetlab
import gidgetlab.abc
import gidgetlab.sansio
from gidgetlab.aiohttp import GitLabAPI, GitLabBot

from .log import make_logger
from .project import FAILURE, SUCCESS, MonitoredProject, ProjectConfigError


class ArtifactBot:
    def __init__(
        self,
        config: Dict[str, Any],
        url: Optional[str] = None,
        username: Optional[str] = None,
        max_workers: int = 3,
        actually_post: bool = True,
    ):
        self.__logger = make_logger(__name__, self)
        self.actually_post = actually_post

        self.url = url or os.getenv("GL_URL")
        self.username = username or os.getenv("GL_USERNAME")
        if not self.username:
            self.__logger.error("No supername supplied!")
            raise RuntimeError("No username supplied")

        self.gl_bot = GitLabBot(self.username, url=self.url)
        trust_hook_data = os.getenv("BOT_TRUST_HOOK_DATA")
        if trust_hook_data is None or trust_hook_data == "":
            self.trust_hook_data = False
        else:
            self.trust_hook_data = True

        self.__logger.debug(
            "Bot initializing up with URL %s and username %s", self.url, self.username
        )

        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=max_workers)
        # self.__logger.debug('config: %s', pformat(config))

        # by name
        self.projects: Dict[str, MonitoredProject] = {}
        self.process_config(config)

        # Add handlers for web hooks.

        self.gl_bot.router.add(self.mr_handler, "Merge Request Hook", action="open")

        for status in [SUCCESS, FAILURE]:
            self.gl_bot.router.add(
                self.pipeline_handler, "Pipeline Hook", status=status
            )

    def process_config(self, config: Mapping[str, Any]):
        """Process (or re-process) configuration.

        Each call should contain a full configuration: projects not included
        will be deactivated.
        """
        # Clear the numeric ID lookup, if known
        self.projects_by_id: Dict[int, MonitoredProject] = {}

        # Filter out comments and meta-info like $schema
        config = {
            k: v
            for k, v in config.items()
            if not k.startswith("#") and not k.startswith("$")
        }

        self.__logger.debug("Processing config: %d projects", len(config))

        for proj_name, proj_config in config.items():
            if proj_name in self.projects:
                # Existing project: Just modify
                try:
                    self.projects[proj_name].process_config(proj_config)
                except ProjectConfigError:
                    self.__logger.warning(
                        "Disabling project %s due to error in re-configuration",
                        proj_name,
                        exc_info=True,
                    )
                    del self.projects[proj_name]
            else:
                # New project, create.
                try:
                    self.projects[proj_name] = MonitoredProject(
                        self, proj_name, proj_config
                    )
                except ProjectConfigError:
                    self.__logger.warning(
                        "Skipping project %s due to instantiation error",
                        proj_name,
                        exc_info=True,
                    )

        # Delete all the projects that weren't in this config.
        deactivated_project_names = set(self.projects.keys()).difference(config.keys())
        for proj_name in deactivated_project_names:
            self.__logger.info(
                "Removing monitor for project %s because it was not in the latest "
                "config.",
                proj_name,
            )
            del self.projects[proj_name]

        if not self.actually_post:
            self.__logger.info("Forcing all projects to not actually post.")
            for proj in self.projects.values():
                proj.actually_post = False
        self.populate_id_lookup()

    def populate_id_lookup(self):
        """Iterate through the projects and update the numeric ID lookup."""
        if len(self.projects) == len(self.projects_by_id):
            # We already have them all
            return

        for proj in self.projects.values():
            proj_id = proj.numeric_id
            if proj_id:
                self.projects_by_id[proj_id] = proj

    def listen(self, port=None):
        """Start listening for web hooks to respond to."""
        self.__logger.info("Start listening for web hooks!")
        self.gl_bot.run(port=port)

    async def _wrapper(self, func):
        async with aiohttp.ClientSession() as session:
            gl = GitLabAPI(
                session,
                self.username,
                url=self.url,
                access_token=os.getenv("GL_ACCESS_TOKEN"),
            )
            self.__logger.debug("about to call user function")
            await func(self, gl)

    def call_with_gitlab(self, func):
        """Call the given function in an async environment.

        Passes this object and a GitLabAPI instance as arguments.
        """
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._wrapper(func))

    def lookup_project_from_hook(self, event: gidgetlab.sansio.Event):
        project_id = event.project_id
        name = event.data["project"]["path_with_namespace"]
        proj = self.projects_by_id.get(project_id)
        if proj:
            self.__logger.debug(
                "found project %s by numeric id: %d", proj.name, project_id
            )

        if not proj:
            proj = self.projects.get(name)
            if proj:
                # OK, we found by name.
                # Update the class accordingly.
                proj.numeric_id = project_id
                self.populate_id_lookup()

        if not proj:
            # Dropping, we don't know this project
            return None

        if proj.name != name or proj.numeric_id != project_id:
            # Dropping, something weird happened.
            self.__logger.warning(
                "Got a mismatch in lookup_project_from_hook: "
                "We think project %s is ID %d, "
                "hook data says %s is ID %d",
                proj.name,
                proj.numeric_id,
                name,
                project_id,
            )
            return None

        return proj

    async def pipeline_handler(
        self,
        event: gidgetlab.sansio.Event,
        gl: gidgetlab.abc.GitLabAPI,
        *args,
        **kwargs
    ):
        proj = self.lookup_project_from_hook(event)
        if not proj:
            return
        pipeline_id = event.object_attributes["id"]

        pipeline_data = None
        if self.trust_hook_data:
            pipeline_data = event.data

        self.__logger.debug(
            "invoking comment_for_pipeline_id for project %s, pipeline %d",
            proj.name,
            pipeline_id,
        )
        await proj.comment_for_pipeline_id(gl, pipeline_id, pipeline_data)

    async def mr_handler(
        self,
        event: gidgetlab.sansio.Event,
        gl: gidgetlab.abc.GitLabAPI,
        *args,
        **kwargs
    ):
        proj = self.lookup_project_from_hook(event)
        if not proj:
            return
        mr_iid = event.object_attributes["iid"]
        self.__logger.debug(
            "invoking process_specified_mr for project %s, mr !%d", proj.name, mr_iid
        )
        await proj.process_specified_mr(gl, (mr_iid,))

    async def call_sync(self, func, *args, **kwargs):
        """
        Call a sync function (in the thread pool) from an async function.
        """
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(self.executor, func, *args, **kwargs)
