# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3

import logging

DEFAULT_SILENT = True


def make_logger(module_name, obj=None, suffix=None, silent=DEFAULT_SILENT):
    """Create and configure a logger to be owned by a class.

    Call like `self.__logger = make_logger(__name__, self)`
    """
    components = [module_name]
    if obj is not None:
        components.append(obj.__class__.__name__)

    if suffix:
        components.append(suffix)

    log = logging.getLogger(".".join(components))

    if silent:
        log.addHandler(logging.NullHandler())
    else:
        log.setLevel(logging.DEBUG)
    return log
