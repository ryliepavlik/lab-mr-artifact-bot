# -*- coding: utf-8 -*-
#
# Copyright (c) 2019, 2021 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>

import json
import logging
import os
from pathlib import Path

from dotenv import load_dotenv

from .bot import ArtifactBot

DEFAULT_LOG_LEVEL = logging.INFO


def configure(config_filenames=None, log_level=None):
    """Configure and return the bot.

    config_filenames is a list of filenames. If None, will use env var
    BOT_CONFIG_FILES or config.json.
    """
    DOTENV_PATH = str(Path(__file__).resolve().parent.parent / ".env")
    load_dotenv(DOTENV_PATH)
    if log_level is None:
        log_level = os.getenv("LOG_LEVEL", None)

    if log_level is None:
        log_level = DEFAULT_LOG_LEVEL

    if isinstance(log_level, str):
        log_level_name = log_level
    else:
        log_level_name = logging.getLevelName(log_level)

    logging.basicConfig(level=log_level)

    log = logging.getLogger(__name__)
    log.info("logging level is %s", logging.getLevelName(log_level_name))

    log.info("dotenv path was %s", DOTENV_PATH)

    if not config_filenames:
        config_var = os.getenv("BOT_CONFIG_FILES", "config.json")
        config_filenames = config_var.split(":")

    config = {}
    for fn in config_filenames:
        with open(fn, encoding="utf-8") as fp:
            config.update(json.load(fp))

    return ArtifactBot(config)


def start_bot(config_filenames=None, log_level=None):
    """Run the bot.

    config_filenames is a list of filenames. If None, will use env var
    BOT_CONFIG_FILES or config.json.
    """
    bot = configure(config_filenames, log_level)
    port = os.getenv("PORT", None)
    if not port:
        port = None
    bot.listen(port)
