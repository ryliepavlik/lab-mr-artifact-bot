# -*- coding: utf-8 -*-
#
# Copyright (c) 2019, 2021 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3.7 or newer

import logging
import re
import urllib.parse
from base64 import standard_b64decode as base64decode
from datetime import datetime
from enum import Enum
from typing import Any, Dict, Mapping, Optional, Set

import gidgetlab
import gidgetlab.abc
import jinja2
import jinja2.exceptions
from cachetools import LRUCache
from jinja2 import Environment
from jinja2.environment import Template

from .log import make_logger
from .util import get_nested

# Default first line of all messages.
INTRO = "Beep Boop! I am a bot and here are your artifact links!"

COMMENT_TAG = "lab-mr-artifact-bot: "

METADATA_COMMENT_RE = re.compile(r"\[//\]: # \(%s(?P<content>[^)]+)\)" % COMMENT_TAG)

# I do not know why GitLab did not use consistent parts of speech here,
# but now they presumably can't fix it without breaking the API.
SUCCESS = "success"
FAILURE = "failed"

CACHED_TEMPLATES = 100


def make_metadata_comment(content):
    return "\n\n[//]: # (%s%s)\n" % (COMMENT_TAG, content)


def find_metadata_comment_in_body(body):
    for line in body.split("\n"):
        match = METADATA_COMMENT_RE.match(line)
        if match:
            return match.group("content")


class ProjectConfigError(RuntimeError):  # noqa
    def __init__(self, msg):
        super().__init__(msg)


class ArtifactAction(Enum):  # noqa
    BROWSE = "browse"
    DOWNLOAD = "raw"
    DISPLAY = "file"

    # This one is special: it causes the path to be ignored entirely.
    LOG = "log"


ARTIFACT_ACTION_LOOKUP_BY_STRING: Mapping[str, ArtifactAction] = {
    x.name: x for x in ArtifactAction
}

KEY_TEMPLATE_SOURCE = "mr_template_source"
KEY_TEMPLATE_FILENAME = "mr_template_filename"
KEY_PROJECT_ID = "project_id"
KEY_ACTUALLY_POST = "actually_post"
KEY_MAX_POSTS = "max_posts_per_mr"

DEFAULT_TEMPLATE_FILENAME = ".artifact-bot.md"


def _mangle_note(msg):
    """Remove non-semantic whitespace for better dupe avoidance."""
    if not msg:
        return None
    lines = [x.strip() for x in msg.split("\n")]
    return "".join([x for x in lines if x])


def _decode_date(date_string) -> Optional[datetime]:
    if date_string is None:
        return None
    return datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%fZ")


class MockJobResult:
    """A job result manufactured when we couldn't find a job of that name."""

    def __init__(self, job_name):
        self.exists = False
        self.name = job_name
        self.has_artifacts = False
        self.success = False
        self.web_url = None

    def make_artifact_link(
        self, title, action: ArtifactAction, artifact_path: Optional[str] = None
    ):
        """Make a GFM/CommonMark link for an action and artifact."""
        return (
            f"*(skipped link referring to job {self.name} "
            + "- job name not found in this pipeline)*"
        )


class JobResult:
    """Wrapper for a job JSON.

    The elements of a pipelink hook data's "builds" array,
    and the elements of the array returned when looking up jobs by pipeline,
    are sadly slightly inconsistent right now,
    but we deal with it.
    """

    @staticmethod
    def parse_job_info_collection(jobinfo):
        """
        Get a dictionary of latest job for each name.

        Provide an array of job information objects.
        """

        job_result_list = [JobResult(j) for j in jobinfo]

        # Get a set containing all the unique job names.
        job_names = {result.name for result in job_result_list}

        # Get the most-recently-finished job for each unique name.
        job_results = {
            # The "max" (latest/most recent) job of a given name
            name: max(result for result in job_result_list if result.name == name)
            # for each unique name
            for name in job_names
        }

        # If the newest job for any given job name is started but not finished,
        # then don't  return any jobs.
        for job in job_results.values():
            if job.started_at and not job.finished_at:
                return None

        return job_results

    def __init__(self, json):
        self.__lazy_logger = None

        self.exists = True
        self.json = json
        self.name = json["name"]
        self.web_url = json["web_url"]
        self.id = json["id"]
        self.pipeline_id = json["pipeline"]["id"]
        self.started_at = _decode_date(json["started_at"])
        self.finished_at = _decode_date(json["finished_at"])
        self.success = json["status"] == SUCCESS

        # If artifacts_file exists and has a non-null filename, then we have
        # actual artifacts.
        # The pipeline event gives a null filename in case of no artifacts,
        # while the jobs-for-pipeline omits the entire artifacts_file object.
        artifacts_filename = get_nested(json, "artifacts_file", "filename")
        self.has_artifacts = artifacts_filename is not None

    @property
    def logger(self) -> logging.Logger:
        """
        Get a lazy-initialized logger for this object.

        We usually don't need it.
        """
        if not self.__lazy_logger:
            self.__lazy_logger = make_logger(__name__, self)
        return self.__lazy_logger

    def make_artifact_url(
        self, action: ArtifactAction, artifact_path: Optional[str] = None
    ):
        """
        Compute a URL to access, with the given action, the artifact
        (or directory) specified.

        If no path is specified, only ArtifactAction.LOG and
        ArtifactAction.BROWSE are legal actions.
        """
        if action == ArtifactAction.LOG:
            # This is a shortcut: The log is just the job's web_url
            return self.web_url

        # TODO sanitize path better than this little wimpy thing!!!
        if artifact_path and ".." in artifact_path:
            self.logger.warning(
                "Given an artifact path containing ..: %s", artifact_path
            )
            return None

        # get rid of leading slashes
        if artifact_path:
            artifact_path = artifact_path.lstrip("/")

        parts = [self.web_url, "artifacts", action.value]
        if artifact_path:
            parts.append(artifact_path)
        return "/".join(parts)

    def make_artifact_link(
        self, title, action: ArtifactAction, artifact_path: Optional[str] = None
    ):
        """Make a GFM/CommonMark link for an action and artifact."""
        if action != ArtifactAction.LOG and not self.has_artifacts:
            return (
                f"*(Skipping link to artifact from job {self.name} "
                + "- job has no artifacts)*"
            )
        url = self.make_artifact_url(action, artifact_path)
        if not url:
            return None

        return "[{}]({})".format(title, url)

    def __str__(self):
        return '[JobResult {} "{}" in pipeline {} finished at {}]'.format(
            self.id, self.name, self.pipeline_id, self.finished_at
        )

    def __gt__(self, other):
        if not self.finished_at:
            # Not finished is always last.
            return False

        if self.finished_at == other.finished_at:
            # Here we need a tie-breaker.
            return self.id > other.id

        # Otherwise, just compare timestamps
        return self.finished_at > other.finished_at


class JobCollectionProxy:
    """Wraps a dict of jobs to substitute a fake instead of a dictionary key failure."""

    def __init__(self, job_dict):
        self.__jobs = job_dict

    def __getitem__(self, name):
        if name in self.__jobs:
            return self.__jobs[name]
        return MockJobResult(name)


def _gitlab_urlencode(s: str):
    """URL encode a string, including the slashes."""
    # safe='' so that it escapes slashes too.
    return urllib.parse.quote(s, safe="")


class MonitoredProject:
    """Represents a single project that is being tracked."""

    def __init__(self, artifact_bot, proj_name: str, proj_config: Mapping[str, Any]):
        self.artifact_bot = artifact_bot
        self.gl_bot = artifact_bot.gl_bot

        self.template_env = Environment(autoescape=True)
        for k, v in ARTIFACT_ACTION_LOOKUP_BY_STRING.items():
            self.template_env.globals[k] = v
        self.template = None

        self.compiled_templates_by_blob_id: LRUCache[str, Template] = LRUCache(
            CACHED_TEMPLATES
        )

        self.name = proj_name
        self.__logger = make_logger(__name__, self, self.name.replace("/", "."))

        self.__logger.debug("creating MonitoredProject for %s", self.name)

        self.__project_id: Optional[int] = None

        # Process the config JSON
        self.process_config(proj_config)

        # use this urlencoded project path as the project ID
        # until we learn what the numeric ID is.
        self.escaped_path = _gitlab_urlencode(self.name)

    @property
    def project_id(self):
        """Get the project ID.

        Returns either the numeric ID, if known,
        otherwise the urlencoded name.
        """
        if not self.__project_id:
            return self.escaped_path
        return self.__project_id

    @project_id.setter
    def project_id(self, val: Optional[int]):
        """Set the ID of this project"""
        if val and isinstance(val, int):
            self.__project_id = val

    @property
    def numeric_id(self):
        """Get the numeric ID of this project, if known, otherwise None."""
        return self.__project_id

    @numeric_id.setter
    def numeric_id(self, val: int):
        """Set the numeric ID of this project."""
        self.project_id = val
        return self.project_id

    async def fetch_template_for_branch(
        self, gl: gidgetlab.abc.GitLabAPI, branch_name: str
    ):
        """
        Fetch and re-compile the template for a target branch name.

        Only re-compiles if necessary.
        """
        if not self.template_path:
            # Intentionally disabled per-branch templates
            return

        fn = _gitlab_urlencode(self.template_path)

        try:
            data = await self.get_gitlab_item(
                gl, f"/repository/files/{fn}", params={"ref": branch_name}
            )
        except gidgetlab.exceptions.BadRequest:
            self.__logger.exception(
                "Could not download template %s from branch %s",
                self.template_path,
                branch_name,
            )
            return

        content = data.get("content")
        blob_id = data.get("blob_id")
        if not content or not blob_id:
            raise RuntimeError(
                "Not enough data in response for template %s from branch %s"
                % (self.template_path, branch_name)
            )
        self.branch_to_template_blob_id[branch_name] = blob_id

        if blob_id not in self.compiled_templates_by_blob_id:
            # Updated or not cached: compile the template.
            source = base64decode(content).decode("utf-8")
            self.__logger.debug(
                "Compiling updated template blob id %s for branch %s:\n%s",
                blob_id,
                branch_name,
                source,
            )
            compiled = self.template_env.from_string(source)

            self.compiled_templates_by_blob_id[blob_id] = compiled

    def get_template_for_branch(self, branch_name: str):
        """Retrieve a cached, compiled template for a target branch name.

        Returns self.template (the generic/fallback) in case of any errors.
        """
        if not self.template_path:
            # Intentionally disabled per-branch templates
            return self.template

        if branch_name not in self.branch_to_template_blob_id:
            # Branch doesn't have a known blob
            return self.template

        return self.compiled_templates_by_blob_id.get(
            self.branch_to_template_blob_id[branch_name], self.template
        )

    def compile_template(self, source=None):
        """Compile the fallback template, if it exists."""
        need_compile = not self.template
        if source and source != self.template_source:
            self.template_source = source
            need_compile = True
        if need_compile and self.template_source:
            self.template = self.template_env.from_string(self.template_source)

    def process_config(self, proj_config: Mapping[str, Any]):
        """Process configuration JSON."""
        self.config = proj_config

        self.template_source = proj_config.get(KEY_TEMPLATE_SOURCE)
        if self.template_source and not isinstance(self.template_source, str):
            # OK, assume it's a list.
            self.template_source = "\n".join(self.template_source)

        self.template = None
        self.compile_template()

        if (
            KEY_TEMPLATE_FILENAME in proj_config
            and proj_config[KEY_TEMPLATE_FILENAME] is None
        ):
            # Disable template fetching by explicit specification of None.
            self.template_path: Optional[str] = None
        else:
            self.template_path = proj_config.get(
                KEY_TEMPLATE_FILENAME, DEFAULT_TEMPLATE_FILENAME
            )

        # Reset these associations since the path may have changed.
        self.branch_to_template_blob_id: Dict[str, str] = {}

        if not self.numeric_id:
            # Try getting from config file, if we still haven't found our ID.
            project_id = proj_config.get(KEY_PROJECT_ID)
            if project_id:
                self.project_id = project_id
                self.__logger.debug(
                    "Got numeric project id from config file: %d", self.project_id
                )

        # Can disable posting in the config file.
        self.actually_post = proj_config.get(KEY_ACTUALLY_POST, True)

        if self.actually_post:
            self.__logger.debug("Actual posting of results is enabled.")
        else:
            self.__logger.info(
                "Actual posting of the automated messages disabled by "
                '"%s": false'
                " in the config section for this project.",
                KEY_ACTUALLY_POST,
            )

        self.max_posts = proj_config.get(KEY_MAX_POSTS)

    async def get_gitlab_item(
        self, gl: gidgetlab.abc.GitLabAPI, url: str, params: Optional[Dict] = None
    ):
        """Wrapper for gl.getitem() that prepends the project ID and does logging."""
        if params is None:
            params = {}
        if not url.startswith("/projects"):
            url = f"/projects/{self.project_id}" + url

        if params:
            self.__logger.debug("GET on %s with params %s", url, str(params))
        else:
            self.__logger.debug("GET on %s", url)
        return await gl.getitem(url, params)

    async def post_gitlab(self, gl: gidgetlab.abc.GitLabAPI, url: str, data=None):
        """Wrapper for gl.post that prepends the project ID and does logging."""
        if not url.startswith("/projects"):
            url = f"/projects/{self.project_id}" + url

        self.__logger.debug("POST to %s of %s", url, data)
        return await gl.post(url, data=data)

    async def delete_gitlab(self, gl: gidgetlab.abc.GitLabAPI, url: str):
        """Wrapper for gl.delete that prepends the project ID and does logging."""
        if not url.startswith("/projects"):
            url = f"/projects/{self.project_id}" + url

        self.__logger.debug("DELETE of %s", url)
        return await gl.delete(url)

    async def get_jobs_for_pipeline(
        self, gl: gidgetlab.abc.GitLabAPI, pipeline_id: int
    ):
        """Get the jobs associated with an ID of a pipeline (execution).

        https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
        """
        self.__logger.debug("asking for job info for pipeline %d", pipeline_id)
        return await self.get_gitlab_item(gl, f"/pipelines/{pipeline_id}/jobs")

    async def get_mrs_for_commit(self, gl: gidgetlab.abc.GitLabAPI, commit_sha: str):
        """Get the MRs associated with a commit.

        https://docs.gitlab.com/ee/api/commits.html#list-merge-requests-associated-with-a-commit
        """
        return await self.get_gitlab_item(
            gl, f"/repository/commits/{commit_sha}/merge_requests"
        )

    async def get_notes_for_mr(self, gl: gidgetlab.abc.GitLabAPI, mr_id: int):
        """Get the notes associated with an MR.

        https://docs.gitlab.com/ee/api/notes.html#list-all-merge-request-notes
        """
        return await self.get_gitlab_item(gl, f"/merge_requests/{mr_id}/notes")

    async def get_open_mrs(self, gl: gidgetlab.abc.GitLabAPI):
        """Get all open merge requests for this project.

        https://docs.gitlab.com/ee/api/merge_requests.html#list-project-merge-requests
        """
        return await self.get_gitlab_item(gl, "/merge_requests", {"state": "opened"})

    async def get_pipelines_for_commit(
        self, gl: gidgetlab.abc.GitLabAPI, commit_sha: str
    ):
        """Get pipelines for a commit.

        https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines
        """
        return await self.get_gitlab_item(gl, "/pipelines", {"sha": commit_sha})

    async def process_specified_mr(self, gl: gidgetlab.abc.GitLabAPI, mrs):
        shas = set()
        for mr in mrs:
            data = await self.get_gitlab_item(gl, f"/merge_requests/{mr}")
            shas.add(data["sha"])
        await self.process_commits(gl, shas)

    async def process_all_open_mr(self, gl: gidgetlab.abc.GitLabAPI):
        shas = set()
        for mr in await self.get_open_mrs(gl):
            shas.add(mr["sha"])
        await self.process_commits(gl, shas)

    async def process_commits(self, gl: gidgetlab.abc.GitLabAPI, commits):
        pipelines: Dict[int, Set[Dict[str, Any]]] = {}
        for commit in commits:
            for pipeline in await self.get_pipelines_for_commit(gl, commit):
                status = pipeline["status"]
                if status != SUCCESS and status != FAILURE:
                    continue
                pipeline_id = pipeline["id"]
                if pipeline_id not in pipelines:
                    pipelines[pipeline_id] = set()
                pipelines[pipeline_id].add(commit)

        self.__logger.debug("Here are your pipelines: %s", str(pipelines))
        for pipeline in pipelines:
            await self.comment_for_pipeline_id(gl, pipeline_id=pipeline)

    async def comment_for_pipeline_id(
        self,
        gl: gidgetlab.abc.GitLabAPI,
        pipeline_id: int,
        commit: Optional[str] = None,
        pipeline_data: Optional[Mapping[str, Any]] = None,
    ):
        """Make a comment on all active merge requests associated with this pipeline.

        Called from the pipeline web hook, but may be invoked separately for manual
        triggering or testing.
        """
        if not pipeline_data:
            url = f"/pipelines/{pipeline_id}"
            pipeline_data = await self.get_gitlab_item(gl, url)

        if not pipeline_data:
            self.__logger.warning("Couldn't get data for pipeline %d", pipeline_id)
            return

        if not commit:
            commit = pipeline_data.get("sha")

        if not commit:
            get_nested(pipeline_data, "object_attributes", "sha")

        if not commit:
            self.__logger.warning("Could not get commit for pipeline %d", pipeline_id)
            return

        merge_requests = await self.get_mrs_for_commit(gl, commit)
        if len(merge_requests) == 0:
            # Bail before additional requests are made if this pipeline status
            # isn't useful.
            self.__logger.debug(
                "Latest commit %s - from pipeline %d - is not associated with any "
                "merge requests.",
                commit,
                pipeline_id,
            )
            return

        jobinfo = await self.get_jobs_for_pipeline(gl, pipeline_id)
        if len(jobinfo) == 0:
            self.__logger.info("Pipeline %d has no jobs.", pipeline_id)
            return

        job_results = JobResult.parse_job_info_collection(jobinfo)
        if not job_results:
            self.__logger.debug("This pipeline has not finished.")
            return

        most_recent_finish = max(
            j.finished_at for j in job_results.values() if j.finished_at
        )

        self.__logger.debug(
            "job_results: %s", ", ".join([str(j) for j in job_results.values()])
        )

        status = pipeline_data["status"]
        pipeline_url = pipeline_data["web_url"]
        template_environment = {
            "jobs": JobCollectionProxy(job_results),
            "commit": commit,
            "status": status,
            "pipeline_id": pipeline_id,
            "pipeline_url": pipeline_url,
            "meta": f"Commit {commit}, pipeline [{pipeline_id}]({pipeline_url})",
        }

        target_branches = {mr["target_branch"] for mr in merge_requests}

        for branch in target_branches:
            await self.fetch_template_for_branch(gl, branch)

        for mr in merge_requests:
            await self.conditionally_post_comment_to_mr(
                gl,
                pipeline_id,
                commit,
                status,
                most_recent_finish,
                template_environment,
                mr,
            )

    def process_template(self, pipeline_id, template, environment: Mapping[str, Any]):
        """Process the template with the job results."""
        if not template:
            self.__logger.warning("No template found!")
            return None
        try:
            output = template.render(environment)
            return output

        except jinja2.exceptions.TemplateRuntimeError:
            self.__logger.warning(
                "template runtime error for pipeline %d, skipping: %s",
                pipeline_id,
                exc_info=True,
            )
            return None

    async def conditionally_post_comment_to_mr(
        self,
        gl: gidgetlab.abc.GitLabAPI,
        pipeline_id: int,
        commit: str,
        status: str,
        finished_at: datetime,
        environment: Mapping[str, Any],
        mr_data: Mapping[str, Any],
    ):
        """
        Post links from this pipeline to the MR described by mr_data, if it is suitable.
        """
        mr_id = mr_data["id"]
        mr_iid = mr_data["iid"]
        mr_title = mr_data["title"]
        self.__logger.debug(
            "pipeline %d is associated with !%d: %s", pipeline_id, mr_id, mr_title
        )

        if mr_data["sha"] != commit:
            self.__logger.debug(
                "ending processing of !%d - "
                "pipeline commit (%s) is not head commit (%s)",
                mr_iid,
                commit,
                mr_data["sha"],
            )
            return
        if mr_data["state"] != "opened":
            self.__logger.debug(
                "ending processing of !%d - not open, state is %s",
                mr_iid,
                mr_data["state"],
            )
            return

        if status == SUCCESS and mr_data["merge_when_pipeline_succeeds"]:
            self.__logger.debug(
                "ending processing of !%d - "
                "pipeline successful and merge already scheduled",
                mr_iid,
            )
            return

        # OK, if we get down here, we should make sure we have a comment here.
        target_branch = mr_data.get("target_branch")
        self.__logger.debug("!%d is set to merge into branch %s", mr_iid, target_branch)
        if not target_branch:
            self.__logger.debug(
                "ending processing of !%d - could not get target branch", mr_iid
            )
            return

        template = self.get_template_for_branch(target_branch)

        text = self.process_template(pipeline_id, template, environment)
        if not text:
            self.__logger.debug(
                "ending processing of !%d - could not process template", mr_iid
            )
            return

        # This is used as a key to de-duplicate: sometimes a pipeline will re-run and
        # it matters, e.g. if the status changes,
        # but if status and sha (commit) are the same, don't bother posting.
        metadata_content = "sha: {} status: {}".format(mr_data["sha"], status)
        text_with_metadata = text + make_metadata_comment(metadata_content)

        # Final check: see if we already have a a comment.
        url = f"/merge_requests/{mr_iid}/notes"
        notes = await self.get_gitlab_item(gl, url)

        minimized_body = _mangle_note(text)
        minimized_body_with_metadata = _mangle_note(text_with_metadata)

        bot_notes = []

        for note in notes:
            # First, compare username
            user = note["author"]["username"]
            if user != self.artifact_bot.username:
                self.__logger.debug(
                    "skip message: user %s is not our user, %s",
                    user,
                    self.artifact_bot.username,
                )
                continue

            # Then, compare exact body text (stripping extra whitespace)
            this_body = note["body"]
            this_minimized_body = _mangle_note(this_body)
            if this_minimized_body in (minimized_body, minimized_body_with_metadata):
                self.__logger.info(
                    "ending processing of !%d - we have already posted an identical "
                    "comment on this MR",
                    mr_iid,
                )
                return

            # Finally, look for a metadata "comment" in the markdown source
            this_metadata = find_metadata_comment_in_body(this_body)
            if not this_metadata:
                # No metadata comment, so not a (recent) bot post
                continue
            # Save for later, if we want to prune
            bot_notes.append(note)
            if this_metadata == metadata_content:
                self.__logger.info(
                    "ending processing of !%d - we have already posted a comment on "
                    'this MR with metadata "%s"',
                    mr_iid,
                    metadata_content,
                )
                return

        if self.max_posts:
            max_before_posting = self.max_posts - 1
            self.__logger.debug(
                "Have a limit of %d posts (plus current one) per MR - this MR has %d",
                max_before_posting,
                len(bot_notes),
            )
            if len(bot_notes) > max_before_posting:
                to_remove = bot_notes[:]
                for _ in range(max_before_posting):
                    to_remove.pop()
                self.__logger.debug("Will remove the oldest %d notes", len(to_remove))
                for doomed_note in to_remove:
                    remove_url = url + f"/{doomed_note['id']}"
                    if self.actually_post:
                        await self.delete_gitlab(gl, remove_url)
                    else:
                        self.__logger.info(
                            "Would normally delete the following MR note: %s",
                            remove_url,
                        )

        if self.actually_post:
            post_data = {
                "body": text_with_metadata,
                "created_at": datetime.isoformat(finished_at),
            }
            self.__logger.info("Posting pipeline %d data to %s", pipeline_id, url)
            await self.post_gitlab(gl, url, data=post_data)
        else:
            self.__logger.info(
                "Would normally post the following pipeline %d data to MR %s:\n%s",
                pipeline_id,
                mr_data["web_url"],
                text,
            )
