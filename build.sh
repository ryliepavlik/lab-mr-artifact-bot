#!/usr/bin/env bash
# Copyright (c) 2022 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0

set -e
buildah_build() {
    if buildah --help |grep -q "  bud"; then
        buildah bud "$@"
    else
        buildah build "$@"
    fi
}

. common.sh

echo "Building image $TAG"

buildah_build --pull --file Dockerfile -t "${TAG}"
