#!/usr/bin/env python3
#
# Copyright (c) 2019 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>
"""Sometimes CI jobs get stuck, especially when interacting with AppVeyor.
This clears stale jobs out automatically, by cancelling all "running" jobs
older than 1 day."""

from datetime import datetime
import sys
from pprint import pprint

from artifact_status.main import configure

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please specify which project to wipe out old jobs from.")
        sys.exit(1)

    project = sys.argv[1]
    maxdays = 1
    now = datetime.now()

    async def prune_pipelines(bot, gl):
        proj = bot.projects[project]
        data = await proj.get_gitlab_item(gl, "/pipelines", {"status": "running"})
        print(len(data))
        for pipeline in data:
            pipeline_id = pipeline["id"]
            pipe_data = await proj.get_gitlab_item(gl, f"/pipelines/{pipeline_id}")
            pprint(pipe_data)

            started = datetime.strptime(
                pipe_data["started_at"], "%Y-%m-%dT%H:%M:%S.%fZ"
            )
            print(started)
            delta = now - started
            print(delta)
            if delta.days > maxdays:
                print("OLD! Cancelling!")
                await proj.post_gitlab(gl, f"/pipelines/{pipeline_id}/cancel")

    bot = configure(log_level="DEBUG")
    bot.call_with_gitlab(prune_pipelines)
