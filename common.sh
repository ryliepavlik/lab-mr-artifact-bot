# Copyright (c) 2022 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0

TAG=registry.gitlab.com/ryliepavlik/lab-mr-artifact-bot:${CI_COMMIT_SHA:-latest}
