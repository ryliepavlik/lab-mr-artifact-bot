# Copyright (c) 2019 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0

# Common top-level requirements - should match requirements.in
aiohttp
gidgetlab
python-dotenv

# Dev tools

# for pip-compile
pip-tools

# Code style
black
Dlint
flake8
flake8-alfred
flake8-bandit
flake8-black
flake8-bugbear
flake8-builtins
flake8-comprehensions
flake8-isort
flake8-logging-format
flake8-mutable
flake8-simplify
flake8-typing-imports

# eventually add:
# flake8-annotations


# Refactoring and editor assist (used by VS Code)
rope
jedi
mypy
pyright
types-cachetools
types-setuptools
