#!/usr/bin/env python3
#
# Copyright (c) 2019, 2021 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>

import sys
from argparse import ArgumentParser

from artifact_status.main import configure

if __name__ == "__main__":
    parser = ArgumentParser(
        description="Posts a pipeline artifact message on the specified MRs"
    )
    parser.add_argument(
        "--mr", "-m", nargs="+", help="Specify a project!mrnumber to process.", type=str
    )
    args = parser.parse_args()
    mr_per_project: dict[str, set[int]] = {}
    if not args.mr:
        print("Must pass at least one MR")
        parser.print_help()
        sys.exit(1)
    for arg in args.mr:
        splitarg = arg.split("!")
        proj = splitarg[0]
        mr = splitarg[1]
        if proj not in mr_per_project:
            mr_per_project[proj] = set()
        mr_per_project[proj].add(int(mr))

    async def run_mrs(bot, gl):
        for proj_name, mr_set in mr_per_project.items():
            await bot.projects[proj_name].process_specified_mr(gl, mr_set)

    bot = configure(log_level="DEBUG")
    bot.call_with_gitlab(run_mrs)
