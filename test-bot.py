#!/usr/bin/env python3
#
# Copyright (c) 2019, 2021 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Rylie Pavlik <rylie.pavlik@collabora.com>

import logging

from artifact_status.main import configure


async def demo(bot, gl):
    logging.getLogger(__name__).info("we are inside the demo function!")
    await bot.projects["rpavlik/test"].comment_for_pipeline_id(gl, 7443)


if __name__ == "__main__":
    bot = configure(log_level="DEBUG")
    bot.call_with_gitlab(demo)
